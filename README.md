# 教程
- [黑马程序员匠心之作|C++教程从0到1入门编程,学习编](https://www.bilibili.com/video/BV1et411b73Z)
- [C++教程-快速入门实践提高](https://www.bilibili.com/video/av710969661)

--笔记--

- 常量
    - #define 通常在文件上方 #define week  7;
    - const 修饰一个变量 const int a = 10;
- 关键字
- 基本数据类型
    - bool
    - char
    - short 
    - int 
    - unsigned int 
    - float
    - double
    - long 
    - unsigned long
    - long long 
- ASCII
- 字符串
    - c风格 char str[] = "hello world";
    - c++风格 string str = "hello world";
- sizeof 函数 查看变量占用的字节大小
- cout << 打印到控制台
- cin >>  控制台键盘输入
- 运算符